#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <HardwareSerial.h>
#include <esp32/rom/rtc.h>

// #define DEBUG false
// #define DEBUG true

/* Wifi */
const char *wifiName = "CHCT P401";
const char *wifiPassword = "68686868";

/* System server */
const String serverPath = "http://139.59.103.50:9292/api";
const String loginPath = serverPath + "/auth/login";
const String logoutPath = serverPath + "/auth/logout";
const String checkinPath = serverPath + "/parking/create";
const String checkoutPath = serverPath + "/parking/update";
const String getTokensPath = serverPath + "/auth/get-tokens";

/* System account */
const String gateEmail = "gate1@gmail.com";
const String gatePassword = "123456789";

/* Auth */
String gAccessToken = "";
String gRefreshToken = "";
unsigned int gAccessTokenExpiredIn = 0;
unsigned int gRefreshTokenExpiredIn = 0;

/* Prototype */
String loginRequest(void);
String logoutRequest(void);
String checkinRequest(String cardCode);
String checkoutRequest(String cardCode);
String getTokensRequest(void);
void connectToWifi(void);
void login(void);
void toggleLed(unsigned int delayInterval);
void printRequestLog(String title, String input, int code, String response);
void printHTTPCode(int code);
void printResetReason(RESET_REASON resetReason);

void setup() {
  Serial.begin(115200);

  Serial2.setRxBufferSize(1024);
  Serial2.setTimeout(50);
  Serial2.begin(115200, SERIAL_8N1, 16, 17);

  Serial.print("\nCPU0 reset reason: ");
  printResetReason(rtc_get_reset_reason(0));
  Serial.print("\nCPU1 reset reason: ");
  printResetReason(rtc_get_reset_reason(1));

  pinMode(LED_BUILTIN, OUTPUT);

  connectToWifi();

  login();
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) {
    toggleLed(300);

    if (Serial2.available()) {
      String stm32Command = Serial2.readString();
      String command = stm32Command.substring(0, stm32Command.indexOf(':'));
      String data = stm32Command.substring(stm32Command.indexOf(':') + 1, stm32Command.length());

      Serial.println("\nReceived STM32 command: " + stm32Command);

      if (command == "IN") {
        String stringCheckinResponse = checkinRequest(data);
        if (stringCheckinResponse != "") {
          JSONVar jsonCheckinResponse = JSON.parse(stringCheckinResponse);

          int statusCode = jsonCheckinResponse["statusCode"];
          if (statusCode == HTTP_CODE_CREATED)  // Thành công
          {
            const char *station = jsonCheckinResponse["data"]["slot"]["station"];
            unsigned int position = jsonCheckinResponse["data"]["slot"]["position"];

            char buffer[50];
            sprintf(buffer, "LOCATION:%s:%i", station, position);

            Serial2.write(buffer);
          } else if (statusCode == HTTP_CODE_FORBIDDEN)  // Không tồn tại thẻ
          {
            Serial2.write("FORBIDDEN");
          } else if (statusCode == HTTP_CODE_CONFLICT)  // Thẻ đang dùng
          {
            Serial2.write("CONFLICT");
          } else if (statusCode == HTTP_CODE_NOT_FOUND)  // Hết chỗ
          {
            Serial2.write("NOT_FOUND");
          } else if (statusCode == HTTP_CODE_UNAUTHORIZED)  // Xem lại đăng nhập
          {
            Serial2.write("UNAUTHORIZED");
          }
        }
      } else if (command == "OUT") {
        String stringCheckoutResponse = checkoutRequest(data);
        if (stringCheckoutResponse != "") {
          JSONVar jsonCheckoutResponse = JSON.parse(stringCheckoutResponse);

          int statusCode = jsonCheckoutResponse["statusCode"];
          if (statusCode == HTTP_CODE_OK)  // Thành công
          {
            const char *stringLocalTimeCheckinAt = jsonCheckoutResponse["data"]["stringLocalTimeCheckinAt"];
            unsigned int cost = jsonCheckoutResponse["data"]["cost"];

            char buffer[50];
            sprintf(buffer, "M%s:%iE", stringLocalTimeCheckinAt, cost);

            Serial2.write(buffer);
          } else if (statusCode == HTTP_CODE_FORBIDDEN)  // Không tồn tại thẻ
          {
            Serial2.write("FORBIDDEN");
          } else if (statusCode == HTTP_CODE_CONFLICT)  // Thẻ đang dùng
          {
            Serial2.write("CONFLICT");
          } else if (statusCode == HTTP_CODE_NOT_FOUND)  // Mất xe
          {
            Serial2.write("NOT_FOUND");
          } else if (statusCode == HTTP_CODE_UNAUTHORIZED)  // Xem lại đăng nhập
          {
            Serial2.write("UNAUTHORIZED");
          }
        } else {
          Serial.println("\nInvalid command!");
        }
      }
    }
  } else {
    digitalWrite(LED_BUILTIN, HIGH);

    connectToWifi();
  }
}

void connectToWifi(void) {
  Serial.printf("\nConnecting to Wifi: %s...............\n", wifiName);

  WiFi.mode(WIFI_STA);
  WiFi.begin(wifiName, wifiPassword);

  int tryOnTimes = 5;
  do {
    unsigned long lastTime = millis();
    while (WiFi.status() != WL_CONNECTED && millis() - lastTime < 10000) {
      toggleLed(100);
    }
  } while (WiFi.status() != WL_CONNECTED && tryOnTimes);  
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("\nConnected to Wifi failed!");

    ESP.restart();
  }

  Serial.println("\nConnected to Wifi successfully!");
  Serial.printf("\nIP address: %s\n", WiFi.localIP().toString().c_str());
  Serial.printf("\nRRSI: %i\n", WiFi.RSSI());
}

void login(void) {
  String stringLoginResult = loginRequest();
  if (stringLoginResult != "") {
    JSONVar jsonLoginResult = JSON.parse(stringLoginResult);

    int statusCode = jsonLoginResult["statusCode"];
    if (statusCode == HTTP_CODE_OK) {  // Thành công
      String accessToken = jsonLoginResult["data"]["tokens"]["accessToken"]["value"];
      gAccessTokenExpiredIn = jsonLoginResult["data"]["tokens"]["accessToken"]["expiredIn"];
      String refreshToken = jsonLoginResult["data"]["tokens"]["refreshToken"]["value"];
      gRefreshTokenExpiredIn = jsonLoginResult["data"]["tokens"]["refreshToken"]["expiredIn"];
      gAccessToken = accessToken;
      gRefreshToken = refreshToken;
    } else if (statusCode == HTTP_CODE_UNAUTHORIZED)  // Xem lại đăng nhập
    {
      Serial.println("UNAUTHORIZED");
    }
  }
}

void toggleLed(unsigned int delayInterval) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));

  delay(delayInterval);
}

String loginRequest(void) {
  HTTPClient http;
  int responseCode;
  int tryOnTimes = 5;
  JSONVar jsonBody;
  jsonBody["email"] = gateEmail;
  jsonBody["password"] = gatePassword;
  String stringBody = JSON.stringify(jsonBody);

  http.begin(loginPath);
  http.addHeader("Content-Type", "application/json");

  do {
    responseCode = http.PUT(stringBody);
    tryOnTimes--;
  } while (responseCode < 0 && tryOnTimes);

  String responseBody = http.getString();
  http.end();

  printRequestLog("LOGIN_REQUEST", stringBody, responseCode, responseBody);

  return responseBody;
}

String logoutRequest(void) {
  HTTPClient http;
  int responseCode;
  int tryOnTimes = 5;

  http.begin(logoutPath);
  http.addHeader("Authorization", "Bearer " + gAccessToken);

  do {
    responseCode = http.GET();
    tryOnTimes--;
  } while (responseCode < 0 && tryOnTimes);

  String responseBody = http.getString();
  http.end();

  printRequestLog("LOGOUT_REQUEST", "accessToken: " + gAccessToken, responseCode, responseBody);

  return responseBody;
}

String checkinRequest(String cardCode) {
  HTTPClient http;
  int responseCode;
  int tryOnTimes = 5;
  JSONVar jsonBody;
  jsonBody["cardCode"] = cardCode;
  String stringBody = JSON.stringify(jsonBody);

  http.begin(checkinPath);
  http.addHeader("Authorization", "Bearer " + gAccessToken);
  http.addHeader("Content-Type", "application/json");

  do {
    responseCode = http.POST(stringBody);
    tryOnTimes--;
  } while (responseCode < 0 && tryOnTimes);

  String responseBody = http.getString();
  http.end();

  printRequestLog("CHECKIN_REQUEST", "accessToken: " + gAccessToken + "\ncardCode: " + cardCode, responseCode, responseBody);

  return responseBody;
}

String checkoutRequest(String cardCode) {
  HTTPClient http;
  int responseCode;
  int tryOnTimes = 5;
  JSONVar jsonBody;
  jsonBody["cardCode"] = cardCode;
  String stringBody = JSON.stringify(jsonBody);

  http.begin(checkoutPath);
  http.addHeader("Authorization", "Bearer " + gAccessToken);
  http.addHeader("Content-Type", "application/json");

  do {
    responseCode = http.PUT(stringBody);
    tryOnTimes--;
  } while (responseCode < 0 && tryOnTimes);

  String responseBody = http.getString();
  http.end();

  printRequestLog("CHECKOUT_REQUEST", "accessToken: " + gAccessToken + "\ncardCode: " + cardCode, responseCode, responseBody);

  return responseBody;
}

String getTokensRequest(void) {
  HTTPClient http;
  int responseCode;
  int tryOnTimes = 5;

  http.begin(getTokensPath);
  http.addHeader("Authorization", "Bearer " + gRefreshToken);

  do {
    responseCode = http.GET();
    tryOnTimes--;
  } while (responseCode < 0 && tryOnTimes);

  String responseBody = http.getString();
  http.end();

  printRequestLog("GET_TOKENS_REQUEST", "refreshToken: " + gRefreshToken, responseCode, responseBody);

  return responseBody;
}

void printRequestLog(String title, String input, int code, String response) {
  Serial.println("\n================" + title + "================");
  Serial.println("\nInput:");
  Serial.println(input);
  Serial.println("\nCode:");
  Serial.print(code);
  Serial.print(" - ");
  printHTTPCode(code);
  Serial.println("\nResponse:");
  Serial.println(response);
  Serial.println("\n=============================================");
}

void printHTTPCode(int code) {
  switch (code) {
    case -1: Serial.println("CONNECTION_REFUSED"); break;
    case -2: Serial.println("SEND_HEADER_FAILED"); break;
    case -3: Serial.println("SEND_PAYLOAD_FAILED"); break;
    case -4: Serial.println("NOT_CONNECTED"); break;
    case -5: Serial.println("CONNECTION_LOST"); break;
    case -6: Serial.println("NO_STREAM"); break;
    case -7: Serial.println("NO_HTTP_SERVER"); break;
    case -8: Serial.println("TOO_LESS_RAM"); break;
    case -9: Serial.println("ENCODING"); break;
    case -10: Serial.println("STREAM_WRITE"); break;
    case -11: Serial.println("READ_TIMEOUT"); break;
    case 200: Serial.println("OK"); break;
    case 201: Serial.println("CREATED"); break;
    case 400: Serial.println("BAD_REQUEST"); break;
    case 401: Serial.println("UNAUTHORIZED"); break;
    case 403: Serial.println("FORBIDDEN"); break;
    case 404: Serial.println("NOT_FOUND"); break;
    case 500: Serial.println("INTERNAL_SERVER_ERROR"); break;
  }
}

void printResetReason(RESET_REASON resetReason) {
  switch (resetReason) {
    case 1: Serial.println("POWERON_RESET"); break;           /**<1,  Vbat power on reset*/
    case 3: Serial.println("SW_RESET"); break;                /**<3,  Software reset digital core*/
    case 4: Serial.println("OWDT_RESET"); break;              /**<4,  Legacy watch dog reset digital core*/
    case 5: Serial.println("DEEPSLEEP_RESET"); break;         /**<5,  Deep Sleep reset digital core*/
    case 6: Serial.println("SDIO_RESET"); break;              /**<6,  Reset by SLC module, reset digital core*/
    case 7: Serial.println("TG0WDT_SYS_RESET"); break;        /**<7,  Timer Group0 Watch dog reset digital core*/
    case 8: Serial.println("TG1WDT_SYS_RESET"); break;        /**<8,  Timer Group1 Watch dog reset digital core*/
    case 9: Serial.println("RTCWDT_SYS_RESET"); break;        /**<9,  RTC Watch dog Reset digital core*/
    case 10: Serial.println("INTRUSION_RESET"); break;        /**<10, Instrusion tested to reset CPU*/
    case 11: Serial.println("TGWDT_CPU_RESET"); break;        /**<11, Time Group reset CPU*/
    case 12: Serial.println("SW_CPU_RESET"); break;           /**<12, Software reset CPU*/
    case 13: Serial.println("RTCWDT_CPU_RESET"); break;       /**<13, RTC Watch dog Reset CPU*/
    case 14: Serial.println("EXT_CPU_RESET"); break;          /**<14, for APP CPU, reseted by PRO CPU*/
    case 15: Serial.println("RTCWDT_BROWN_OUT_RESET"); break; /**<15, Reset when the vdd voltage is not stable*/
    case 16: Serial.println("RTCWDT_RTC_RESET"); break;       /**<16, RTC Watch dog reset digital core and rtc module*/
    default: Serial.println("NO_MEAN");
  }
}